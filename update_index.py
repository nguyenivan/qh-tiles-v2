#!/usr/bin/env python
#
# Copyright 2010 TimeZlicer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
from google.appengine.ext import db
from google.appengine.ext import deferred
from google.appengine.runtime import DeadlineExceededError
import models


def update_index(query=models.Project.all(), cursor=None, queue=[]):
  try:
    logging.info("update_index")
    
    if cursor:
      query.with_cursor(cursor)

    for entity in query:
      queue.append(entity)

    db.put(queue)
    logging.info('update_index: completed')
  except DeadlineExceededError:
    deferred.defer(update_index, query, query.cursor(), queue, batch)
