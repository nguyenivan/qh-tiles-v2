#!/usr/bin/env python
import datetime
import os
from google.appengine.ext import deferred
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp import util
from google.appengine.ext.db import BadKeyError
from models import Project
import update_index
import urllib


class Search(webapp.RequestHandler):
	def get(self):
		path = os.path.join(os.path.dirname(__file__), 'search.html')
		self.response.out.write(template.render(path, None))

	def post(self):
		search = self.request.get('search')
		t = datetime.datetime.now()
		results = Project.all().search(search).fetch(10)
		template_values = {
			'results': results,
			'elapsed': datetime.datetime.now() - t, 
			'search': search,
			'results_count': len(results)
		}

		path = os.path.join(os.path.dirname(__file__), 'search.html')
		self.response.out.write(template.render(path, template_values))

class InsertHandler(webapp.RequestHandler):
	def get(self):
		pid = urllib.unquote(self.request.get("pid"))
		p = Project.gql("WHERE pid = :1", pid).count()
		if p < 1:
			name = urllib.unquote(self.request.get("name"))
			description = urllib.unquote(self.request.get("desc"))
			type = urllib.unquote(self.request.get("type"))
			district = urllib.unquote(self.request.get("district"))
			city = urllib.unquote(self.request.get("city"))
			lat = urllib.unquote(self.request.get("lat"))
			lon = urllib.unquote(self.request.get("lon"))
			address = urllib.unquote(self.request.get("address"))
			zoom = urllib.unquote(self.request.get("zoom"))
			link = urllib.unquote(self.request.get("link"))
			investor = urllib.unquote(self.request.get("investor"))
			if pid is not None and name is not None and type is not None and district is not None and city is not None and zoom is not None and link is not None:
				proj = Project()
				proj.pid = pid
				proj.name = name
				proj.description = description
				proj.type = type
				proj.investor = investor
				proj.address = address
				proj.district = district
				proj.city = city
				proj.lat = lat
				proj.lon = lon
				proj.link = link
				proj.zoom = zoom
				proj.put()
				self.response.out.write("ok")
			else:
				self.response.out.write("Fail: "+pid)
		else:
			self.response.out.write("ok")

application = webapp.WSGIApplication([
	('/asfw', InsertHandler),
	('/', Search)
	], debug=True)

def main():
		util.run_wsgi_app(application)

if __name__ == '__main__':
		main()
