﻿import csv
import urllib2
import urllib
import threading
import time, datetime

print "Initializing..."
re = csv.reader(open('projects.csv', 'rb'), delimiter=';', quotechar='"')

numOfQueue = 10
queue = []

def sendFile(pid, name, desc, type, district, city, lat, lon, address, link, zoom, investor):
	req = "?pid="+urllib.quote(pid)+"&name="+urllib.quote(name)
	req += "&desc="+urllib.quote(desc)+"&type="+urllib.quote(type)
	req += "&district="+urllib.quote(district)+"&city="+urllib.quote(city)
	req += "&lat="+urllib.quote(lat)+"&lon="+urllib.quote(lon)
	req += "&address="+urllib.quote(address)+"&link="+urllib.quote(link)
	req += "&zoom="+urllib.quote(zoom)+"&investor="+urllib.quote(investor)
	r = urllib2.urlopen("http://localhost:8895/asfw"+req)
	ok = r.read()
	if ok != "ok":
		print ok

class MyThread(threading.Thread):
	def __init__(self, threadID, arr):
		self.threadID = threadID
		self.arr = arr
		threading.Thread.__init__(self)
	def run(self):
		for item in self.arr:
			sendFile(item[0], item[1], item[2], item[3], item[4], item[5], item[6], item[7], item[8], item[9], item[10], item[11])

def addToQueue(id, pid, name, desc, type, district, city, lat, lon, address, link, zoom, investor):
	while len(queue) <= id:
		queue.append([])
	queue[id].append([pid, name, desc, type, district, city, lat, lon, address, link, zoom, investor])

# Start upload code

count = 0
for row in re:
	addToQueue(count, row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11])
	count = count+1
	if count > numOfQueue-1:
		count = 0

print "Starting uploader threads..."

numOfQueue = len(queue)

thread = []
i = 0
while len(thread) < numOfQueue:
	thread.append(MyThread(i, queue[i]))
	i = i+1
	if i >=numOfQueue:
		i = 0

for th in thread:
	th.start()

still = numOfQueue

print "Uploading..."

while still != 0:
	i = 0
	for th in thread:
		if not th.isAlive():
			still = still-1
			thread.pop(i)
		i = i+1
	#time.sleep(1)
	pass

print "Finished"