#!/usr/bin/env python
#
# Copyright 2010 TimeZlicer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#		 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from google.appengine.ext import db

import tzsearch

class Project(tzsearch.SearchableModel):
	pid = db.StringProperty()
	name = db.StringProperty()
	description = db.StringProperty()
	type = db.StringProperty()
	district = db.StringProperty()
	city = db.StringProperty()
	lat = db.StringProperty()
	lon = db.StringProperty()
	address = db.StringProperty()
	link = db.StringProperty()
	zoom = db.StringProperty()
	investor = db.StringProperty()
	
